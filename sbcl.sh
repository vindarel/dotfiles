
#!/bin/bash

rlwrap -i -f lisp-completions.txt \
       --remember \
       --complete-filenames \
       --break-chars "(){}[],'#\";|\\" \
       --quote-characters "\"" \
       --multi-line='  ' \
       sbcl $*
