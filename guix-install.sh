#!/bin/bash

# http://dustycloud.org/blog/guix-package-manager-without-make-install/

#########
# Goal: install the Guix package manager, but not globally (without make install).
# Final usage:
# $ guix-enable   # run this whenever you want to play with guix things
# $ guix package -i xeyes
#########

INSTALL_DIR=~/projets
cd $INSTALL_DIR

# git clone the repo and cd into it
git clone git://git.savannah.gnu.org/guix.git
cd guix

# Install appropriate dependencies... insert your distro package manager here
# Not actually sure if -dev needed on libgcrypt20 or not :)
sudo apt-get install guile-2.0 libgcrypt20 libgcrypt20-dev build-essential \
    guile-gnutls guile-2.0-dev libsqlite3-dev pkg-config autopoint

# Build the package (and don't run make install)
./bootstrap && ./configure && make
# this will take ages. It will build guile objects (not binaries) of
# all guix packages: audacity, abiword, python, everything (see
# gnu/packages/).  See binary installation:
# https://gnu.org/software/guix/manual/guix.html#Binary-Installation
# A pach to make it faster is supposed to land on the ML soon.

# Make the "worker users" and their group... this allows the daemon
# to offload package building while keeping things nicely contained
sudo groupadd guix-builder
for i in `seq 1 10`; do
    sudo useradd -g guix-builder -G guix-builder           \
                 -d /var/empty -s `sudo which nologin`          \
                 -c "Guix build user $i" --system          \
                 guix-builder$i;
done

# Make the /gnu/store directory, where packages are kept/built
sudo mkdir -p /gnu/store
sudo chgrp guix-builder /gnu/store
sudo chmod 1775 /gnu/store


sudo ./pre-inst-env guix archive --authorize < hydra.gnu.org.pub



# Guix stuff (for .bashrc)
function guix-enable() {

    alias guix="$INSTALL_DIR/guix/pre-inst-env guix"
    alias guix-daemon="sudo $INSTALL_DIR/guix/pre-inst-env guix-daemon --build-users-group=guix-builder"

    # add guix's bin to the path
    export PATH=$HOME/.guix-profile/bin:$PATH
    # and others
    export PYTHONPATH="$HOME/.guix-profile/lib/python3.4/site-packages"
    export GUILE_LOAD_PATH="$GUILE_LOAD_PATH:$HOME/.guix-profile/share/guile/site/2.0/"
    export GUILE_LOAD_COMPILED_PATH="$GUILE_LOAD_PATH:$HOME/.guix-profile/share/guile/site/2.0/"
}

# and see the blog post to configure emacs.
