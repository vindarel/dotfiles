#! /bin/bash

# Load private stuff:
source ~/dotfiles/private-dotfiles/privaterc.sh

alias cdp='cd ..'
alias cdpp='cd ../..'
alias cdm='cd -'
alias eog="eog >/dev/null 2>/dev/null"
alias exaile="exaile >/dev/null 2>/dev/null"
export EDITOR=vim
export LESS='-R'
export LESSOPEN='|~/.lessfilter %s' #utiliser avec python-pygmentize
#taper 'v' pour ouvrir le fichier dans l'éditeur.
export HISTIGNORE="&:ls:cd:lh"
export HISTCONTROL=ignorespace
export HISTSIZE=1000000
export HISTFILESIZE=1000000
# ainsi les commandes commencées par un espace sont ignorées par l'historique
# "ignorespace", "ignoredups", "ignoreboth" or "erasedups". Bash 2.05 doesn't have "erasedups".
# shopt -s histappend             # historiques concaténés: non.

alias ssh-palpitante="ssh root@92.222.73.7"
alias ssh-digitalocean="ssh root@165.22.24.83"

mfind () {
    play=0
    # pour pouvoir avoir plusieurs instances de mfind sans se marcher
    #  sur la meme playlist
    ma_playlist=ma_playlist$(date '+%T')
    if [ $# -eq '0' ] ; then
        echo "usage : mfind mots clefs"
        echo "option -p : jouer les fichiers musicaux avec mplayer"
        echo "option -c n : lister les fichiers modifiés depuis moins de n jours "
        echo "Recherche les mots clefs dans le nom complet des fichiers présents dans l'arborescence"
        return
    fi

    arg=''; n=''; ctime=''
    #echo "rajouter -x pour | xargs $programme !"; sleep 1 #nov 11
    while [ $# -gt '0' ] ; do
        if [ $1 = "-p" ] ; then
            echo play \!
            play=1

        else
            if [ $1 = "-c" ] ; then
                ctime='-ctime'
                shift

                if [ $# -eq 0 ] ; then
                    # echo il manque le nombre de jours \!
                    n="-1" # le nombre de jours est 1 par défaut
                    # return
                # fi

                else
                    n="-$1"
                fi

                if [ $2 = '-p' ] ; then
                    n="-1"
                fi
                #echo ctime \! $n

            else
                arg=$arg*$1
            fi
        fi
        shift
    done

    arg=$arg'*'
    #echo arg=$arg
    # ne pas effacer ma_playlist, pour permettre plusieurs instances
    #   de mfind
    #cecho $j c quoi ma_playlist \? : $ma_playlist
    # if [ -f $ma_playlist ] ; then
    #     cecho $r est bien un fichier
    #     #$ma_playlist="$ma_playlist$(date $T)"
    # fi


    #-iname cherche dans seulement le nom de fichier
    #-iwholename dans le nom complet
    # find . $ctime $n -iwholename "$arg" |sort > $ma_playlist
    find . $ctime $n -iwholename "$arg"  > $ma_playlist # -print0 | xargs -0 mplayer
    cat $ma_playlist
    if [ "$play" -eq "1" ] ; then
        mpv -ao pulse -playlist $ma_playlist
    fi
    rm $ma_playlist
}

alias mp="mfind -c 1 -p"
#alias pfind='mfind -c 1 -p'
pfind() {
    mfind -c 1000 -p $@
}

# dicos 10
# sauvegarder le mot recherché en latex ...
esfr() {
    elinks http://www.wordreference.com/esfr/$1
}
fres() {
    elinks http://www.wordreference.com/fres/$1
}
fren() {
    elinks http://www.wordreference.com/fren/$1
}
enfr() {
    elinks http://www.wordreference.com/enfr/$1
}
ende(){
    elinks http://www.wordreference.com/ende/$1
}
deen(){
    elinks http://www.wordreference.com/deen/$1
}

defr(){
    echo "Cet ordre n'est pas possible sur wordreference !"
}

frpt(){
    elinks http://www.systranet.fr/dictionnaire/francais-portugais/$1
}
ptfr(){
    elinks http://www.systranet.fr/dictionnaire/portugais-francais/$1
}

mshuffle() {
    # Si une instance de mplayer tourne déja, la tuer
    #ps -ax|grep mplayer && killall mplayer
    # ... ce qui a l'air assez inutile car dans tous les cas il y a le
    #  processus de grep ! donc on tue tjrs le processus. Autant écrire
    # moyen pour ne pas utiliser de fichier intermédiaire :
    #  find . -iname "*.mp3" -exec mplayer {} + ... avec xargs ??

    # A faire : permettre de suivre les liens symboliques : avec -L
    #  les arguments du script sont les arguments de find : typiquement, -L
    # peut être : chemin=$($1=+"./") très approximativement !

    #si on est dans le home, ce qui arrive à chq démarrage, alors on va dans le repertoire de musique !
    if [ $(pwd) = "/home/vinced" ] ; then
	cd ~/zique
    fi

    #REP=/tmp/  # pb des repertoires interdits en ecriture, comme root ou un disque
    REP=
    #killall mplayer && cecho $r "un processus mplayer a été tué"
    # find -L : suivre les liens symboliques
    find $@ ./ -iname "*.mp3" -o -iname "*.wma" -o -iname "*.mp4" -o -iname "*.wmv" -o -iname "*.ogg" -o -iname "*.m4a" -o -iname "*.wav" \
-o -iname "*flv" -o -iname "flash*" -o -iname "*mkv" |grep -i -v INCOM|grep -v Russe>${REP}ma_playlist
    # lecture
    # ao pulse : pour résoudre audio device got stuck
    mpv -ao pulse -shuffle -playlist ${REP}ma_playlist
    rm ${REP}ma_playlist
# possibilité : rajouter le fait de pouvoir exclure des repertoires de la recherche (voir man find)
#  (avec -wholename et -prune)
#  et ne faire un shuffle qu'avec des répertoires voulus.
# et qu'avec des genres voulus ... => Rhythmbox !
}

############################################################################
# alias vers des radios
############################################################################

# -ao pulse pour lmde

alias bbc4='cecho $j BBC Radio 4; cecho $b voir ~/.radios/notes pour plus de radios;echo taper \<espace\> pour mettre sur pause;echo q pour quitter; mplayer -ao pulse rtsp://rmlive.bbc.co.uk/bbc-rbs/rmlive/ev7/live24/radio4/live/r4_dsat_g2.ra '
alias groovalizacion='mplayer -ao pulse -playlist http://icecast.arscenic.tv:8000/groovalizacion.m3u'

alias russianVoice='mplayer -ao pulse -playlist http://playerservices.streamtheworld.com/m3u/RUVR_KAV.m3u'
alias goloc_rassia='mplayer -ao pulse -playlist http://playerservices.streamtheworld.com/m3u/RUVR_KAV.m3u'
alias radio_libertaire='mplayer -ao pulse -playlist http://ecoutez.radio-libertaire.org:8080/radiolib.m3u'
alias libertaire=radio_libertaire
alias radio_kaleidoscope='mplayer -ao pulse http://81.56.112.26:8000/listen.pls'
alias kaleidoscope=radio_kaleidoscope
alias br_radioMuda='mplayer -ao pulse http://orelha.radiolivre.org:8000/muda.ogg'
alias muda_br='mplayer -ao pulse http://orelha.radiolivre.org:8000/muda.ogg'
alias br_universidade_poa='mplayer -ao pulse -playlist http://143.54.58.13:8000/stream.mp3.m3u'
alias br_nacional_rio='mplayer -ao pulse  http://radioslivres.radiobras.gov.br:8080/nacionalrio.mp3' #news culture
alias br_radio_usp='mplayer -ao pulse -playlist http://www.emm.usp.br/radiouspw.asx'#http://www.radio.usp.br/

alias fip='mpv http://direct.fipradio.fr/live/fip-midfi.mp3'
alias culture='mpv http://direct.franceculture.fr/live/franceculture-midfi.mp3'
alias radio_culture='mpv http://direct.franceculture.fr/live/franceculture-midfi.mp3'
alias canal_sud="mpv http://91.224.148.160:8000/canalsud-live"
alias radio_canal_sud="mpv http://91.224.148.160:8000/canalsud-live"
alias radio_amalia_fado='mplayer -ao pulse mms://stream.radio.com.pt/ROLI-ENC-496'
alias radio_fado=radio_amalia_fado #de amalia.fm
alias radio_russian_vera='mplayer -ao pulse -playlist http://www.am1470.com/livefm.asx'
alias radio_allemagne_kulture='mplayer -ao pulse -playlist http://www.dradio.de/streaming/dkultur.asx'
alias radio_allemagne_munich='mplayer -ao pulse -playlist http://streams.br-online.de/b5plus_2.asx'
alias lora_munchen="mplayer http://live.lora924.de:8000/lora.ogg"
alias radio_lora_munchen="mplayer http://live.lora924.de:8000/lora.ogg"

# SomaFM http://somafm.com
alias radio_somafm_sf_ambiance_police="mpv http://somafm.com/m3u/sf1033130.m3u"
alias radio_somafm_goa="mpv http://somafm.com/m3u/suburbsofgoa130.m3u"

alias stoner_eule='mplayer -ao pulse -playlist http://yp.shoutcast.com/sbin/tunein-station.pls?id=104308'
alias metal_wfenec='echo "a ecouter sur le site'
alias kexp='mplayer -playlist http://live-mp3-128.kexp.org:8000/listen.pls'

alias firefox='firefox 2>/dev/null 1>/dev/null'
alias thunderbird='thunderbird 2>/dev/null 1>/dev/null'
alias deluge='deluge 2>/dev/null 1>/dev/null'
alias epiphany='epiphany >/dev/null'

myradios(){
    cecho $b Mes radios disponibles :
    echo -e "\t campusgrenoblehelp"
    echo -e "\t franceinterhelp"
    echo -e "\t groovalizacion"
    echo -e "\t russianVoice, goloc_rassia --cassé"
#    echo -e " \t antena_negra_Br, br_* "
    echo -e "\t radio_libertaire"
    echo -e "\t canal sud"
    echo -e "\t br radio Muda"
    echo -e "\t br nacional Rio"
    echo -e "\t br radio USP  http://www.radio.usp.br/"
    echo -e "\t fip"
    echo -e "\t culture (marche avec radioTray)"
    echo -e "\t radio amalia, fado"
    echo -e "\t allemagne_*"
    echo -e "\t stoner"
    echo -e "\t metal_wfenec"
    echo -e "\t kexp"
    echo -e "\t lora (Munchen)"

}
alias my-radios=myradios


###################################################################################
#maintenant, mai 09, on peut toujours ouvrir un fichier dans emacs meme s'il existe déja une session !
#  on utiliser emacsclient -n (-n pour redonner la main derrière)
#  on lance le server avec (server-start) dans notre .emacs

# Utiliser emacs --daemon ?! cf http://emacs-fu.blogspot.com/2009/02/emacs-daemon.html
# résoudre interference avec mode terminal
 cemacs () { #un alias ne peut pas executer un petit script, d'ou une fonction
 if (ps|grep emacs); then
       cecho $j "Attention, il y a deja une session emacs  en cours ! :)";
       #echo  "mais on ouvre ces fichiers quand même dans la même session"
      #sleep 1
      #emacsclient -n $@ &
      #fg %emacs
 else emacs -nw $@
 fi
}

# idem avec vim
 cvim () { #un alias ne peut pas executer un petit script, d'ou une fonction
 if (ps|grep vim); then
    echo "Attention, il y a deja une session vim  en cours ! :)";
 else vim $@
 fi
}

alias t='tree -d'
alias tl='tree -d|less'
# bind: bash only.
# bind -x '"\C-x\C-n":nautilus $(pwd)  1>/dev/null 2>/dev/null &'
# bind -x '"\C-x\C-e":fg %emacs' #pour relancer emacs rapidement
alias grep='grep -i --color'
alias igrep='grep -i --color'
# bind -x '"\C-x\C-a":fg %vim' #pour relancer vim
alias "cd-"='cd -'
alias ls='ls --group-directories-first --color'
alias lh='ls -lh'
alias l='k' # zsh plugin
alias la='ls -ah'
alias lal='ls -alh'
alias df='df -h'
alias pax='ps ax | grep'
alias liferea='liferea 2>/dev/null'
alias emacs="emacs -mm" # maximised
alias evince="xreader"

alias xtar="tar xvf" # extract tar
alias untar="tar xvf" # extract tar

# colored man pages nov 11
export LESS_TERMCAP_mb=$'\E[01;31m'

export LESS_TERMCAP_md=$'\E[01;37m'

export LESS_TERMCAP_me=$'\E[0m'

export LESS_TERMCAP_se=$'\E[0m'

export LESS_TERMCAP_so=$'\E[01;44;33m'

export LESS_TERMCAP_ue=$'\E[0m'

export LESS_TERMCAP_us=$'\E[01;32m'

# ou alors :
#export PAGER="/usr/bin/most" #trop de rouge

extract ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       rar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.tar.gz)    tar xvf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via extract()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Bookmark the current directory:
b() { alias $1=cd\ $PWD; }

# Activer un virtualenv
alias activate="source ./bin/activate"
alias wo="workon"

# ledger journal:
export LEDGER_FILE="~/.hledger.journal"

alias e="emacsclient -n"
alias ytmps="mpsyt"
alias youtube-mps="mpsyt"
alias hp="history | percol"
alias ftstop="pkill -STOP thunderbird ; pkill -STOP firefox"
alias ftstart="pkill -CONT firefox ; pkill -CONT thunderbird"
alias livescript="lsc"
alias bepow='xmodmap -e "keycode 28 = w W w W egrave egrave egrave Egrave"'
alias bepoè='xmodmap -e "keycode 28 = w W egrave W egrave egrave egrave Egrave"'

alias dg="python3 -m dg "

function savefilms() { rsync -avzr --progress $@  /media/$USER/Elements/films/}
alias rsyncfilms=savefilms

function rvm-enable () {
    # for diaspora/ruby dev, but I don't want this at every new shell (don't cluster zsh prompt).
    # https://wiki.diasporafoundation.org/Installation/Debian/Jessie?db=postgres&mode=development#Versions_of_this_guide
    [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
}

alias fr="setxkbmap fr -variant bepo;  xmodmap ~/.Xmodmap"
alias eo="setxkbmap fr"
# Nim, choosenim, installed with
# curl https://nim-lang.org/choosenim/init.sh -sSf | sh
export PATH=~/.nimble/bin:$PATH

# env variables pour Abelujo make publish. Needs ssh key upstream.
# export ABELUJO_USER= see private-dotfiles/privaterc.sh
export ABELUJO_SERVER="ssh1.yulpa.io"
export ABELUJO_HTDOCS="/datas/vol3/w4a152947/var/www/dev.abelujo.cc/htdocs/"

export ABELUJO_WEBSITE_HTDOCS="/datas/vol3/w4a152947/var/www/abelujo.cc/htdocs/"
