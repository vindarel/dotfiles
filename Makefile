#!/bin/make

USER=vince

all: python pip sudo fasd zsh node npm lisp
PHONY: all

min:
	@sudo ./requirements/apt-min.sh

apt-packages:
	grep -v "#" requirements/apt-all.txt | xargs sudo apt-get install -y --force-yes

debian7-min: sudo debian7-emacs
	@sudo apt-get install -y vim terminator htop aptitude

debian7-emacs: sudo
	@grep "wheezy-backports" /etc/apt/sources.list || (sudo echo "deb http://http.debian.net/debian wheezy-backports main" && sudo apt-get update && sudo apt-get -t wheezy-backports install -y emacs24)
	@echo "emacs24 is installed."


python:
	./requirements/python.sh

pip: python
	sudo pip install -r requirements/pip.txt

pyenv:
	# pyenv installer
	@cd vendor/ && curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

sudo:
	#su -c 'sh -c "echo \"group ALL=(vince) NOPASSWD: ALL\" >> /etc/sudoers"'
	@id | grep "sudo" || (su -c "adduser vince  sudo" && echo "You must re-connect to your session to have sudo powers.")
	@echo "sudo: done. Re-connect to your session to have sudo powers."

zshtools:
	[ -e /bin/zsh ] || sudo apt-get install zsh
	wget --no-check-certificate http://install.ohmyz.sh -O - | sh
	cd && mv .zshrc .zshrc.pre-dotfiles && ln -s dotfiles/.zshrc
	# default shell
	@chsh ${USER} --shell /bin/zsh  # needs user's pwd
	# get k extension
	git clone https://github.com/supercrabtree/k $HOME/.oh-my-zsh/custom/plugins/k


zshrc:
	@cd && ln -s dotfiles/.zshrc

zsh: zshtools zshrc

fasd:
	sudo cp vendor/fasd /usr/local/bin && sudo chmod +x /usr/local/bin/fasd

# curses-based fuzzy selector. Installs shortcuts.
fzf:
	git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
	~/.fzf/install

emacs-all: emacs.d emacs-cask

emacs.d:
	bash -c "[[ -d ~/.emacs.d/my-elisp ]] || (cd ~/.emacs.d/ && git clone https://gitlab.com/emacs-stuff/my-elisp.git )"
	@cd ~/.emacs.d/ && ln -s my-elisp/init.el

emacs-cask:
	@curl -fsSL https://raw.githubusercontent.com/cask/cask/master/go | python

vimperator:
	cd && ln -s dotfiles/.vimperatorrc

vim-vundle:
	git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle

vimrc:
	cd && ln -s dotfiles/.vimrc

# project manager
prm:
	# prm is tracked by gws and git modules in vendor/
	cd && ln -s dotfiles/.prm

node:
	@sudo apt-get install nodejs npm nodejs-legacy

node-update:
	# https://askubuntu.com/questions/426750/how-can-i-update-my-nodejs-to-the-latest-version#480642
	curl -sL https://deb.nodesource.com/setup_4.x | bash -

npm:
	@grep -v "^#" requirements/npm.txt | xargs sudo npm install -g

latex:
	@grep -v "^#" requirements/latex.txt | xargs sudo apt-get install -y

lisp:
	@grep -v "^#" requirements/lisp.txt | xargs sudo apt-get install -y
	cd && ln -s dotfiles/.sbclrc  # untested

# Thanks lispkit Makefile.
# apt-get install cl-quicklisp
# plus
# sbcl --script /usr/share/cl-quicklisp/quicklisp.lips
LISP ?= sbcl
QL_LOCAL=$(PWD)/.quicklocal/quicklisp
QUICKLISP_SCRIPT=http://beta.quicklisp.org/quicklisp.lisp
QL_OPTS=--load $(QL_LOCAL)/setup.lisp
LOCAL_OPTS=--noinform --noprint --disable-debugger --no-sysinit --no-userinit
quicklisp: lisp
	@curl -O $(QUICKLISP_SCRIPT)
	@sbcl $(LOCAL_OPTS) \
		--load quicklisp.lisp \
		--eval '(quicklisp-quickstart:install :path "$(QL_LOCAL)")' \
		--eval '(quit)'


# CLisp shell friendly. Dead ?
# https://github.com/fukamachi/shelly
shelly: lisp
	@curl -L http://shlyfile.org/shly | /bin/sh # doesn't find quicklisp

lispkit:
	cd vendor && test -d lispkit || git clone https://github.com/AeroNotix/lispkit
	cd vendor/lispkit/ && make

lisp-roswell:
	./roswell-install.sh
	# We can download and share user scripts with roswell.
	echo "export PATH=\"\$HOME/.roswell/bin:\$PATH\"" >> .zshrc

roswell: lisp-roswell

mao:
	@grep -v "^#" requirements/apt-mao.txt | xargs sudo apt-get install -y

gws:
	@cd vendor/ && wget https://raw.githubusercontent.com/StreakyCobra/gws/master/src/gws


# The Elixir programming language, from Erlang. Highly functional and
#  concurrent. http://elixir-lang.org
elixir:
	@echo "solution from may 2015"
	@wget http://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && sudo dpkg -i erlang-solutions_1.0_all.deb
	@rm erlang-solutions_1.0_all.deb
	@sudo apt-get update
	@sudo apt-get install -y elixir
	@echo "not found ? replace squeeze by wheezy in sources.list.d/erlang-solutions"

docker:
	@curl -sSL https://get.docker.com/ | sh
	@su -c 'sh -c "curl -L https://raw.githubusercontent.com/docker/compose/1.1.0/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose"'
	@echo "Install docker-compose via pip. May need to update completions (currently v.1.1)."

ruby: ruby-apt ruby-gems

ruby-apt:
	@grep -v "^#" requirements/ruby/apt-ruby.txt | xargs sudo apt-get install -y
ruby-gems:
	@grep -v "^#" requirements/ruby/ruby-packages.txt | xargs sudo gem install

smalltalk: pharo

pharo:
	cd vendor/ && sudo curl get.pharo.org/64 | bash

tmsu:
	# cli files tagging tools
	./install-tmsu.sh

# clojure mao repl https://daveyarwood.github.io/alda/2015/09/05/alda-a-manifesto-and-gentle-introduction/
alda:
	sudo curl https://raw.githubusercontent.com/alda-lang/alda/master/bin/alda -o /usr/local/bin/alda && sudo chmod +x /usr/local/bin/alda
	chmod +x vendor/boot && sudo mv vendor/boot /usr/local/bin/

zeronet:
	@wget https://github.com/HelloZeroNet/ZeroNet/archive/master.tar.gz
	@tar xvpfz master.tar.gz
	cd ZeroNet-master
	# python zeronet.py

	# with docker: docker run -p 15441:15441 -p 43110:43110 nofish/zeronet

libervia:
	wget https://repos.goffi.org/sat_docs/raw-file/tip/docker/libervia_cont.sh && chmod a+x libervia_cont.sh
	./libervia_cont.sh

palemoon: # run with sudo
	# lightweight firefox web browser
	echo 'deb http://download.opensuse.org/repositories/home:/stevenpusser/Debian_8.0/ /' >> /etc/apt/sources.list.d/palemoon.list
	apt-get update
	apt-get install palemoon

shotcut16.05:
	wget https://github.com/mltframework/shotcut/releases/download/v16.05/shotcut-debian7-x86_64-160501.tar.bz2
	tar -xvf shotcut* vendor/

wisp: # parens-less lisp
	hg clone http://draketo.de/proj/wisp vendor/wisp
	cd vendor/wisp && autoreconf -i && ./configure && make

aspell:
	# there's a mismatch of dictionnaries in emacs' aspell
	sudo ln -s /usr/lib/aspell/de_DE.alias /usr/lib/aspell/deutsch.alias
guilerc:
	cd && ln -s dotfiles/.guile

guile-colorized:
	git clone https://github.com/NalaGinrut/guile-colorized vendor/guile-colorized
	cd vendor/guile-colorized && sudo make install

ricin:
	# tox-based messaging app
	@echo "downloading appimage…"
	cd vendor && wget https://cdn.ricin.im/linux/ricin-0.2.1.app
	cd vendor && chmod a+x ricin-0.2.1.app
	# cd vendor && ./ricin-0.2.1.app

lxd-jessie:
	# Virtualization. Looks so better than Docker.
	@echo "lxd is included in Ubuntu 16.04 LTS. We need liblxc1 >= 2 and golang-go>=1.5. Both are in jessie backports."
	$echo "Adding jessie backports"
	echo "deb http://ftp2.fr.debian.org/debian/ jessie-backports main" >> /etc/apt/sources.list.d/additional-repositories.list
	@echo "Installing lxc and go (190Mo) from backports."
	sudo apt-get -t jessie-backports install lxc-dev lxc golang-go squashfs-tools xz-utils

xiki:
	# Mind blowing shell
	curl -L https://xiki.com/install_xsh -o vendor/install_xsh; bash vendor/install_xsh

selenium-geckodriver:
	cd vendor/ && wget https://github.com/mozilla/geckodriver/releases/download/v0.11.1/geckodriver-v0.11.1-linux64.tar.gz && tar xzvf geckodriver*.tar.gz

crystal:
	curl -sSL https://dist.crystal-lang.org/apt/setup.sh | sudo bash
	sudo apt install crystal # 178Mo
	apt-get install libc6-dev libevent-dev libpcre2-dev libpng-dev libssl1.0-dev libyaml-dev zlib1g-dev # Lucky framework
	# libpcre2-dev and libssl1 not found. libpcre3-dev installed.
	# overmind process manager.
