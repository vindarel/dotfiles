jslint
httpserver
tern
jsonlint
livescript
prelude-ls
elementor
gulp
bower
html2jade
# process data with livescript syntax
ramda-cli
ipt 	# interactive pipe, selection from list
ied 	# faster package installer
markdown-toc 	# works with same headings too
dat 	# distributed data sharing
pm2 	# process manager for nodejs, builtin load balancer
