#!/bin/bash

# Install python dependencies
sudo apt-get install -y python-pip virtualenvwrapper ipython ipython-qtconsole \
    python2.7-dev \
    libxml2-dev libxslt1-dev
# pyenv
# https://github.com/yyuu/pyenv#installation
