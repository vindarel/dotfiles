# ZSH configuration

# Install oh-my-zsh in the home.
# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
# ZSH_THEME="robbyrussell"
ZSH_THEME="gallois"
# ZSH_THEME="agnoster" # like vim's powerline
# ZSH_THEME="simple"
# DEFAULT_USER=""

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable bi-weekly auto-update checks
DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# k: install separately
plugins=(git fabric npm bower docker gem httpie pip sudo supervisor mix k)
# others: pyenv (completion works atm)
# sudo: ESC twice to add sudo

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=~/bin:$PATH

source ~/dotfiles/demarrer.sh

PATH=~/dotfiles/scripts:"$PATH"

export PATH="$HOME/dotfiles/vendor/:$PATH"
eval "$(fasd --init auto)"
eval "$(fasd --init posix-alias zsh-hook zsh-ccomp zsh-ccomp-install zsh-wcomp zsh-wcomp-install)"
alias v='f -e vim' # quick opening files with vim
alias m='f -e mplayer' # quick opening files with mplayer
alias o='a -e xdg-open' # quick opening files with xdg-open
alias r="fasd -d -e ranger" # browse directories super fast
alias d='fasd_cd -d'     # cd, same functionality as j in autojump



# virtualenv wrapper
# http://www.rosslaird.com/blog/first-steps-with-mezzanine/
export WORKON_HOME=$HOME/.virtualenvs
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
export PIP_VIRTUALENV_BASE=$WORKON_HOME
# alias deactivate="source deactivate"

# ssh-ident python package:
# use multiple ssh profiles for sites like gitlab.
# https://github.com/ccontavalli/ssh-ident
# dec 14
# I don't need it with a good .ssh/config and .git/config with ssh aliases.
# alias ssh=/home/vince/projets/ssh-ident/ssh-ident
# alias git='BINARY_SSH=git /home/vince/projets/ssh-ident/ssh-ident'

# pyenv: python environment installer and switcher.
# https://github.com/yyuu/pyenv-installer
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# selecta and zsh: insert found paths directly into the shell command line.
# By default, ^S freezes terminal output and ^Q resumes it. Disable that so
# that those keys can be used for other things.
unsetopt flowcontrol
# Run Selecta in the current working directory, appending the selected path, if
# any, to the current command.
function insert-selecta-path-in-command-line() {
    local selected_path
    # Print a newline or we'll clobber the old prompt.
    echo
    # Find the path; abort if the user doesn't select anything.
    selected_path=$(find * -type d | selecta) || return
    # Append the selection to the current command buffer.
    eval 'LBUFFER="$LBUFFER$selected_path"'
    # Redraw the prompt since Selecta has drawn several new lines of text.
    zle reset-prompt
}
# Create the zle widget
zle -N insert-selecta-path-in-command-line
# Bind the key to the newly created widget
bindkey "^S" "insert-selecta-path-in-command-line"

# More:
# Export pyenv to the load path (switch python versions)
 export PATH="$HOME/.pyenv/bin:$PATH"
 eval "$(pyenv init -)"
 eval "$(pyenv virtualenv-init -)"

# Cask, test runner for emacs.
export PATH="/home/$USER/.cask/bin:$PATH"

# bepow
# change keyboard layout (bug in LMDE2)
setxkbmap fr -variant bepo
# xmodmap -e "keycode 28 = w W w W egrave egrave egrave Egrave"
# make caps lock another control:
# xmodmap -e "keycode 66 = Control_L Control_L Control_L Control_L"

xmodmap ~/.Xmodmap

export PATH="/usr/local/p/versions/python:$PATH"
# prm, a simple project manager
alias prm=". ~/dotfiles/vendor/prm/prm.sh"


# do NOT share history between consoles, like by default with oh-my-zsh
unsetopt share_history
stty -ixon

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# For a server, connection by emacs' Tramp.
# case "$TERM" in
# "dumb")
    # PS1="> "
    # ;;
# esac

# add lxd to path (installed manually)
export PATH="$PATH:$HOME/.go/bin/" #xxx
alias lxd="$GOPATH/bin/lxd"
alias lxc=$GOPATH/bin/lxc


# source ~/.xsh  # too long ? I don't use it anyway.

export PATH="$HOME/.roswell/bin:$PATH"

# guix is installed with their script.
export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
