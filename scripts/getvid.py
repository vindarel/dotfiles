#!/usr/bin/env python

import os
import sys

CMD = "youtube-dl -o '%(title)s-%(id)s{tags}.%(ext)s'"

def main(url, *args):
    if args:
        tags = "-" + "-".join(*args)
    cmd = CMD.format(tags=tags) + " " + url
    os.system(cmd)
    # tmsu

if __name__ == "__main__":
   exit(main(sys.argv[1], sys.argv[2::]))
