#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

from bs4 import BeautifulSoup as bs


def get_extension_id(soup):
    """Get the id of a rdf file.
    """
    ext_id = None
    try:
        ext_id = soup.Description.targetApplication.find_all("id")[0].text
    except Exception as e:
        print "Error while parsing the rdf file: {}".format(e)

    return ext_id

def get_name(soup):
    return soup.Description.find_all("name")[0].text

def main(path_to_rdf):
    with open(path_to_rdf, "rb") as f:
        soup = bs(f.read(), features="xml")

    ext_id = get_extension_id(soup)
    ext_name = get_name(soup)
    print (ext_name, ext_id)


if __name__ == "__main__":
    path_to_rdf = sys.argv[1]
    if os.path.isdir(path_to_rdf):
        path_to_rdf = os.path.join(path_to_rdf, "install.rdf")
        if not os.path.exists(path_to_rdf):
            print "no install.rdf found in {}".format(path_to_rdf)
    main(path_to_rdf)
