#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os

"""
uplist: list from the given up to the newest.
TODO: cli args, everything proprer
"""

oldest = 0
mlist = []
uplist = []
max_age_allowed = 1000000000000000000000000000
EXCLUDE = [".jpg", ".gif"]

import sys
reverse = True  # newest to oldest
upwards = False
if len(sys.argv) > 1 and sys.argv[1]:
    if "-r" == sys.argv[1]:
        del(sys.argv[1])
        reverse = False
    if len(sys.argv) > 1 and "-u" == sys.argv[1]:
        upwards = True
        del(sys.argv[1])
if len(sys.argv) > 1 and sys.argv[1]:
    max_age_allowed = os.stat(sys.argv[1]).st_mtime + 1

for dirname,subdirs,files in os.walk("."):
    for fname in files:
        n, ext = os.path.splitext(fname)
        if not ext in EXCLUDE:
            full_path = os.path.join(dirname, fname)
            try:
                mtime = os.stat(full_path).st_mtime
            except:
                continue # maybe a symlink
            if (mtime < max_age_allowed):
                # go downwards, to the oldest.
                if not fname.endswith("~") or (not fname.endswith("pyc")):
                    mlist.append((mtime, full_path))
            else:
                if upwards:
                    uplist.append((mtime, full_path))


if upwards:
    mlist = uplist
sorted_list = sorted(mlist, reverse=reverse)
print "\n".join('"%s"' % (f[1],) for f in sorted_list)
