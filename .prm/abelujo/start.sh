#!/usr/bin/env bash

# This script will run when STARTING the project "abelujo"
# Here you might want to cd into your project directory, activate virtualenvs, etc.

cd ~/projets/ruche-web/abelujo
workon abelujo
