#!/bin/bash

# Install Roswell. Common lisp.
# https://github.com/roswell/roswell/wiki/1.-Installation

if which apt-get > /dev/null; then sudo apt-get -y install git build-essential automake libcurl4-openssl-dev;fi
git clone -b release https://github.com/roswell/roswell.git ~/dotfiles/vendor/roswell
cd ~/dotfiles/vendor/roswell
sh bootstrap
./configure
make
sudo make install
ros setup
