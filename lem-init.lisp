;; For Lem 2.0
;; copy or symlink to ~/.lem/init.lisp
;;
;; $ cd lem/
;;  CL_SOURCE_REGISTRY=$(pwd)// ros -s lem-sdl2 -e '(lem:lem)'

;;;
;;; vi-mode
;;;
;;; Missing keys:
;;; M-j newline with comment (if inside comment)
;;; M-h select paragraph
;;;
;;; Issues:
;;; can't type backslash or any key with Alt Gr (right Alt key)
;;; typing an accent like éppriints sa spacee after it and sorta repaeat keys Oo
;;;
;;; Lisp mode:
;;; C-c C-y run function at point (with package prefix if necessary)
;;;
;;; Nice to have:
;;; inside M-: be able to use M-. to go to definition.

;;; See:
;;; https://gitlab.com/sasanidas/lem-config/-/blob/master/init.lisp

;; We want some more CL libraries.
(pushnew "/home/vince/projets/cl-str/" asdf:*central-registry* :test #'equal)

;; (eval-when (:compile-toplevel :load-toplevel :execute)
  ;; (ql:quickload "str"))

(in-package :lem-user)

;; Evaluating Lisp in M-: we want to be in Lem' package.
(lem-lisp-mode/internal::lisp-set-package "LEM")

;; Start in vi-mode
(lem-vi-mode:vi-mode)

;;;
;;; Some helper functions, bound to keys below.
;;;
;;;
(define-command beginning-of-defun-on-function (n) ("p")
  "Go to the beginning of defun, the point on the function name."
  ;; if n < 0, go to end of defun.
  (lem/language-mode::beginning-of-defun-1 n)
  (lem-vi-mode/word:forward-word-end (current-point) n t)
  (skip-whitespace-forward (current-point) t))

(define-key lem-vi-mode:*command-keymap*
  "g a"
  'beginning-of-defun-on-function)

;; <2023-05-25 Thu>
;; vi-mode doesn't have "+" and "-" keys.
;;
;; We could find the variables and command names thanks to Lem's autocompletion.
(define-key lem-vi-mode:*command-keymap*
  "-"
  'lem-vi-mode/commands:vi-previous-line)


(define-key lem-vi-mode:*command-keymap*
  "+"
  'lem-vi-mode/commands:vi-next-line)

(define-key lem-vi-mode:*command-keymap*
  "q"
  'kill-buffer)

(define-key lem-vi-mode:*command-keymap*
  "Space"
  'lem-vi-mode/commands:vi-forward-char)

(define-key lem-vi-mode:*command-keymap*
  "("
  'backward-paragraph)

;; doesn't work, inserts a ) or says "no matching can be inserted"
;; fixed upstream after a ping in Discord :)
(define-key lem-vi-mode:*command-keymap*
  ")"
  'forward-paragraph)

;; Lem as completion with abbrev.
;; C-p is bound to abbrev
;; As suggested by its example, we define C-n to display a list of suggestions.
;; It is originally bound to next-line, in vi-mode too.
(define-key lem-vi-mode:*insert-keymap* "C-n" 'lem/abbrev:abbrev-with-pop-up-window)
(define-key *global-keymap* "C-n" 'lem/abbrev:abbrev-with-pop-up-window)

;;
;; Some keys of my liking:
;;
(define-key lem-vi-mode:*command-keymap*
  "' b"
  'select-buffer)

;; (only nice on a bepo keyboard)
(define-key lem-vi-mode:*command-keymap*
  "' «"
  'previous-buffer)

(define-key lem-vi-mode:*command-keymap*
  "' »"
  'next-buffer)

(define-key lem-vi-mode:*command-keymap*
  ">"
  'move-to-end-of-buffer)

(define-key lem-vi-mode:*command-keymap*
  "<"
  'move-to-beginning-of-buffer)

(define-key lem-vi-mode:*command-keymap*
  "M-»"
  'move-to-end-of-buffer)

(define-key lem-vi-mode:*command-keymap*
  "M-«"
  'move-to-beginning-of-buffer)

;;; vi visual mode
(define-key lem-vi-mode/visual::*visual-keymap*
  "x"
  'lem:kill-region)

;;;
;;; Global keymap
;;;
(define-key *global-keymap*
  "F6"
  'lem/language-mode::comment-or-uncomment-region)

;; these work both on normal and vi mode... not sure.
(define-key *global-keymap*
  "M-s"
  'previous-line)
(define-key *global-keymap*
  "M-t"
  'next-line)

;;;
;;; Project
;;;
(define-key *global-keymap*
  "C-x p f"
  'lem-core/commands/project:project-find-file)

(define-command find-directory-buffer () ()
  (let ((name (buffer-filename)))
    (and
     (switch-to-buffer
      (find-file-buffer (lem-core/commands/file::directory-for-file-or-lose (buffer-directory))))
     ;; check buffer name exists as a file.
     (when name
       (search-forward (current-point) name)
       (window-recenter (current-window))
       (backward-char (length name))))))

(define-key *global-keymap*
  "C-x C-j"
  'find-directory-buffer)

;;;
;;; Misc
;;;
(define-command open-init-file () ()
  ;; @sasanidas
  (lem:find-file
   (merge-pathnames "init.lisp" (lem-home))))

(define-command oif () ()
  (open-init-file))
