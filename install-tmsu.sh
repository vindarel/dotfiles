#!/bin/bash

cd vendor
REL=$(curl https://api.github.com/repos/oniony/tmsu/releases/latest | jq -r ".assets[1].browser_download_url") # -r : strip quotes
echo "We suppose $REL is for x86"
wget $REL
tar xvf tmsu*.tgz
cd tmsu*/
sudo cp bin/tmsu /usr/bin
sudo cp misc/zsh/_tmsu /usr/share/zsh/site-functions # working ?
